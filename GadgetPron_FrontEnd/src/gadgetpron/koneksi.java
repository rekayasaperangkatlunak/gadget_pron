/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gadgetpron;

/**
 *
 * @author AureLia
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import javax.swing.JOptionPane;

public class koneksi {
    Connection con;
    Statement stm;
    
    public void config() {
        try {
            String url = "jdbc:sqlite:data.db";
            Class.forName("org.sqlite.JDBC");
            con = DriverManager.getConnection(url);
            System.out.println("Connection has been established.");
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
        finally {
            try {
                if (con != null)
                {
                    con.close();
                }
            }
            catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }
}

package gadgetpron;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;

public class Database {
     
    private Connection connect() {
        // SQLite connection string
        String url = "jdbc:sqlite:C://sqlite/db/databaseuser.db";
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return conn;
    }
     
    //Class create new database user
     public static void createNewDatabaseUser(String fileName) {
 
        String url = "jdbc:sqlite:C:/sqlite/db/" + fileName;
        
        // SQL statement for creating a new table
        String sql = "CREATE TABLE IF NOT EXISTS User (\n"
                + "	id integer PRIMARY KEY,\n"
                + "	username text NOT NULL,\n"
                + "	password text NOT NULL,\n"
                +"      nama text NOT NULL\n"
                + ");";
        
        try (Connection conn = DriverManager.getConnection(url);
                Statement stmt = conn.createStatement()) {
            // create a new table
            stmt.execute(sql);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
         try (Connection conn = DriverManager.getConnection(url)) {
            if (conn != null) {
                DatabaseMetaData meta = conn.getMetaData();
                System.out.println("The driver name is " + meta.getDriverName());
                System.out.println("A new database has been created.");
            }
 
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
     
     //fungsi untuk check login
     public boolean login(String username, String password){
         boolean Status = false;
         String unData = null;
         String PwdData = null;
             
         String sql = "Select username,password FROM User where username = " +"'"+ username+ "'";
       
         try (Connection conn = this.connect();
             Statement stmt  = conn.createStatement();
             ResultSet rs    = stmt.executeQuery(sql)){
            
            // loop through the result set
             System.out.println(rs.getString("username") + "\t" +
                                rs.getString("password")
             );
             unData=rs.getString("username");
             PwdData=rs.getString("password");
         
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }  
         if(username.equals(unData) && password.equals(PwdData)){
             JOptionPane.showMessageDialog(null, "Welcome "+username);
             return true;
         }
         else{
             JOptionPane.showMessageDialog(null, "Username atau password salah");
             return false;
         }
     }
     
     //fungsi untuk sign in
     public boolean SignUp(String username, String password, String nama) {
        String sql = "INSERT INTO User(username,password,nama) VALUES(?,?,?)";
 
        try (Connection conn = this.connect();
            PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, username);
            pstmt.setString(2, password);
            pstmt.setString(3, nama);
            pstmt.executeUpdate();
            JOptionPane.showMessageDialog(null, "Data Berhasil Ditambah");
            return true;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return false;
    }

     
     //untuk membuat tabel smartphone
     public static void createNewDatabaseSmartphone(String fileName) {
 
        String url = "jdbc:sqlite:C:/sqlite/db/" + fileName;
        
        // SQL statement for creating a new table
        String sql = "CREATE TABLE IF NOT EXISTS Smartphone (\n"
                + "	id integer PRIMARY KEY,\n"
                + "	merek text NOT NULL,\n"
                + "	seri text NOT NULL,\n"
                +"      kamera integer NOT NULL,\n"
                +"      ram integer NOT NULL,\n"
                +"      memory integer NOT NULL,\n"
                +"      processor text NOT NULL,\n"
                +"      battery   integer NOT NULL,\n"
                +"      layar integer NOT NULL,\n"
                +"      warna text NOT NULL,\n"
                +"      harga integer NOT NULL,\n"
                +"      fitur text NOT NULL,\n"
                +"      sinyal text NOT NULL\n"
                + ");";
        
        try (Connection conn = DriverManager.getConnection(url);
                Statement stmt = conn.createStatement()) {
            // create a new table
            stmt.execute(sql);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
         try (Connection conn = DriverManager.getConnection(url)) {
            if (conn != null) {
                DatabaseMetaData meta = conn.getMetaData();
                System.out.println("The driver name is " + meta.getDriverName());
                System.out.println("A new database has been created.");
            }
 
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

    }
     
     
     public ArrayList<Smartphone> selectAll(){
        Smartphone smart;
        ArrayList<Smartphone> list = new ArrayList<>();                
        String sql = "SELECT id, merek, seri, kamera,ram,memory,processor,battery,layar,"
                + "warna,harga,fitur,sinyal FROM Smartphone";
        
        try (Connection conn = this.connect();
             Statement stmt  = conn.createStatement();
             ResultSet rs    = stmt.executeQuery(sql)){
            
            // loop through the result set
            while (rs.next()) {
                smart = new Smartphone();
                smart.setId(rs.getInt("id"));
                smart.setHarga(rs.getInt("harga"));
                smart.setKamera(rs.getInt("kamera"));
                smart.setLayar(rs.getInt("layar"));
                smart.setMerek(rs.getString("merek"));
                smart.setRam(rs.getInt("ram"));
                smart.setMemory(rs.getInt("memory"));
                smart.setSeri(rs.getString("seri"));
//                System.out.println(rs.getString("merek") +  "\t" + 
//                                   rs.getString("seri") + "\t" +
//                                   rs.getString("kamera") + "\t" +
//                                   rs.getString("ram") + "\t" +
//                                   rs.getString("processor") + "\t" +
//                                   rs.getString("battery") + "\t" +
//                                   rs.getString("layar") + "\t" +
//                                   rs.getString("warna") + "\t" +
//                                   rs.getString("harga") + "\t" +
//                                   rs.getString("fitur") + "\t" +
//                                   rs.getString("sinyal"));
                list.add(smart);
            }
           return list;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return null;
        } 
    }
    
    public void selectItem(String nama){
        String sql = "Select id, merek, seri, kamera,ram,memory,processor,battery,layar,"
                + "warna,harga,fitur,sinyal FROM Smartphone WHERE nama LIKE "+ "'%"+nama+"'";
         try (Connection conn = this.connect();
             Statement stmt  = conn.createStatement();
             ResultSet rs    = stmt.executeQuery(sql)){
            
            // loop through the result set
                System.out.println(rs.getString("merek") +  "\t" + 
                                   rs.getString("seri") + "\t" +
                                   rs.getInt("kamera") + "\t" +
                                   rs.getInt("ram") + "\t" +
                                   rs.getInt("memory") + "\t" +
                                   rs.getString("processor") + "\t" +
                                   rs.getInt("battery") + "\t" +
                                   rs.getInt("layar") + "\t" +
                                   rs.getString("warna") + "\t" +
                                   rs.getInt("harga") + "\t" +
                                   rs.getString("fitur") + "\t" +
                                   rs.getString("sinyal") + "\t" );

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    public ArrayList<Smartphone>search(String input){
        Smartphone smart;
        ArrayList<Smartphone> list = new ArrayList<>();                
        String sql = "SELECT merek, seri, kamera,ram,memory,processor,battery,layar,"
                + "warna,harga,fitur,sinyal FROM Smartphone "
                + "Where merek like"+"'%"+input+"%' OR"
                + " seri like"+"'%"+input+"%'";
        
        try (Connection conn = this.connect();
             Statement stmt  = conn.createStatement();
             ResultSet rs    = stmt.executeQuery(sql)){
            
            // loop through the result set
            while (rs.next()) {
                smart = new Smartphone();
                smart.setHarga(rs.getInt("harga"));
                smart.setKamera(rs.getInt("kamera"));
                smart.setLayar(rs.getInt("layar"));
                smart.setMerek(rs.getString("merek"));
                smart.setRam(rs.getInt("ram"));
                smart.setMemory(rs.getInt("memory"));
                smart.setSeri(rs.getString("seri"));
//                System.out.println(rs.getString("merek") +  "\t" + 
//                                   rs.getString("seri") + "\t" +
//                                   rs.getString("kamera") + "\t" +
//                                   rs.getString("ram") + "\t" +
//                                   rs.getString("processor") + "\t" +
//                                   rs.getString("battery") + "\t" +
//                                   rs.getString("layar") + "\t" +
//                                   rs.getString("warna") + "\t" +
//                                   rs.getString("harga") + "\t" +
//                                   rs.getString("fitur") + "\t" +
//                                   rs.getString("sinyal"));
                list.add(smart);
            }
           return list;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return null;
        } 
    }
    
    public ArrayList<Smartphone>searchAdvance(String merek, String ram,String kamera, String memory, String layar, String harga){
        Smartphone smart;
        ArrayList<Smartphone> list = new ArrayList<>();
        String TempRam = null;
        String TempKamera = null;
        String TempMemory = null;
        String TempLayar = null;
        String TempHarga = null;
        
        switch(ram){
            case "<= 4 GB":TempRam = "ram <= 4 AND";
                           break;
            case ">= 6 GB":TempRam = "ram >= 6 AND";
                           break;
        }
        
        switch(kamera){
            case "<= 8 MP":TempKamera = "kamera <= 8 AND";
                          break;
            case ">= 10 MP":TempKamera = "kamera >=10 AND";
                            break;
        }
        switch(memory){
            case "<= 16 GB":TempMemory = "memory <= 16 AND";
                          break;
            case ">= 32 GB":TempMemory = "memory >= 32 AND";
                            break;
        }
        switch(layar){
            case "<= 6":TempLayar = "layar <= 8 AND";
                          break;
            case "> 6":TempLayar = "layar >=10 AND";
                            break;
        }
        switch(harga){
            case "<= Rp 5.000.000":TempHarga = "harga <= 5000000 AND";
                          break;
            case ">= Rp 6.000.000 - <= Rp. 10.000.000":TempHarga = "harga >= 6000000 AND harga <= 10000000 AND";
                                                        break;
            case ">= Rp 12.000.000":TempHarga = "harga >= 12000000";
                                     break;
        }
        
        
        String sql = "SELECT merek, seri, kamera, ram, memory, processor, battery, layar,"
                + "warna, harga, fitur, sinyal FROM Smartphone "
                + "Where merek like"+"'%"+ merek +"%' AND"
                + TempRam + TempKamera + TempMemory + TempLayar + TempHarga;
        
        try (Connection conn = this.connect();
             Statement stmt  = conn.createStatement();
             ResultSet rs    = stmt.executeQuery(sql)){
            
            // loop through the result set
            while (rs.next()) {
                smart = new Smartphone();
                smart.setHarga(rs.getInt("harga"));
                smart.setKamera(rs.getInt("kamera"));
                smart.setLayar(rs.getInt("layar"));
                smart.setMerek(rs.getString("merek"));
                smart.setRam(rs.getInt("ram"));
                smart.setMemory(rs.getInt("memory"));
                smart.setSeri(rs.getString("seri"));
//                System.out.println(rs.getString("merek") +  "\t" + 
//                                   rs.getString("seri") + "\t" +
//                                   rs.getString("kamera") + "\t" +
//                                   rs.getString("ram") + "\t" +
//                                   rs.getString("processor") + "\t" +
//                                   rs.getString("battery") + "\t" +
//                                   rs.getString("layar") + "\t" +
//                                   rs.getString("warna") + "\t" +
//                                   rs.getString("harga") + "\t" +
//                                   rs.getString("fitur") + "\t" +
//                                   rs.getString("sinyal"));
                list.add(smart);
            }
            return list;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return null;
        } 
    }
    
    public void insert(String merek, String seri, int kamera,int ram,int memory,String processor
    ,int battery,int layar,String warna,int harga,String fitur,String sinyal) {
        String sql = "INSERT INTO Smartphone(merek,seri,kamera,ram,memory,processor,"
                + "battery,layar,warna,harga,fitur,sinyal) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
 
        try (Connection conn = this.connect();
            PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, merek);
            pstmt.setString(2, seri);
            pstmt.setInt(3, kamera);
            pstmt.setInt(4, ram);
            pstmt.setInt(5, memory);
            pstmt.setString(6, processor);
            pstmt.setInt(7, battery);
            pstmt.setInt(8, layar);
            pstmt.setString(9, warna);
            pstmt.setInt(10, harga);
            pstmt.setString(11, fitur);
            pstmt.setString(12,sinyal);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
      
    public void delete(int id) {
        String sql = "DELETE FROM Smartphone WHERE id = " + id ;
 
        try (Connection conn = this.connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
 
            // set the corresponding param
            // pstmt.setString(1, seri);
            // execute the delete statement
            pstmt.executeUpdate();
 
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
      
    public void update(String merek, String seri, int kamera,int ram,int memory,String processor
    ,int battery,int layar,String warna,int harga,String fitur,String sinyal,int id) {
        String sql = "UPDATE Smartphone SET "
                + "merek = ? , "
                + "seri = ? ,"
                +"kamera = ?,"
                +"ram = ?,"
                +"memory = ?,"
                +"Processor = ?,"
                +"battery = ?,"
                +"layar = ?,"
                +"warna = ?,"
                +"harga = ?,"
                +"fitur= ?,"
                +"sinyal = ?"
                + "WHERE id = "+id;
        System.out.println(id);
        try (Connection conn = this.connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
 
            // set the corresponding param
            pstmt.setString(1, merek);
            pstmt.setString(2, seri);
            pstmt.setInt(3, kamera);
            pstmt.setInt(4, ram);
            pstmt.setInt(5, memory);
            pstmt.setString(6, processor);
            pstmt.setInt(7, battery);
            pstmt.setInt(8, layar);
            pstmt.setString(9, warna);
            pstmt.setInt(10, harga);
            pstmt.setString(11, fitur);
            pstmt.setString(12, sinyal);
            // update 
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
     public static void main(String[] args) {
    }

    void update(String samsung, String s5, String mp, String string, String qualcom, String _mah, String _inch, String putih, String string0, String radio, String g, String s50) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    void insert(String merek, String seri, int kamera, int ram, String processor, int memory, int battery, int layar, String warna, int harga, String fitur, String sinyal) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}

package gadgetpron;
public class Smartphone {

    private String merek;
    private String seri;
    private int kamera;
    private int ram;
    private int memory;
    private int layar;
    private int harga;
    private int baterai;
    private String fitur;
    private String sinyal;
    private String processor;
    private String warna;
    private int id;

    
    /**
     * @return the memory
     */
    public int getMemory() {
        return memory;
    }

    /**
     * @param memory the memory to set
     */
    public void setMemory(int memory) {
        this.memory = memory;
    }
    
    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    /**
     * @return the merek
     */
    public String getMerek() {
        return merek;
    }

    /**
     * @param merek the merek to set
     */
    public void setMerek(String merek) {
        this.merek = merek;
    }

    /**
     * @return the seri
     */
    public String getSeri() {
        return seri;
    }

    /**
     * @param seri the seri to set
     */
    public void setSeri(String seri) {
        this.seri = seri;
    }

    /**
     * @return the kamera
     */
    public int getKamera() {
        return kamera;
    }

    /**
     * @param kamera the kamera to set
     */
    public void setKamera(int kamera) {
        this.kamera = kamera;
    }

    /**
     * @return the ram
     */
    public int getRam() {
        return ram;
    }

    /**
     * @param ram the ram to set
     */
    public void setRam(int ram) {
        this.ram = ram;
    }

    /**
     * @return the layar
     */
    public int getLayar() {
        return layar;
    }

    /**
     * @param layar the layar to set
     */
    public void setLayar(int layar) {
        this.layar = layar;
    }

    /**
     * @return the harga
     */
    public int getHarga() {
        return harga;
    }

    /**
     * @param harga the harga to set
     */
    public void setHarga(int harga) {
        this.harga = harga;
    }

    /**
     * @return the baterai
     */
    public int getBaterai() {
        return baterai;
    }

    /**
     * @param baterai the baterai to set
     */
    public void setBaterai(int baterai) {
        this.baterai = baterai;
    }

    /**
     * @return the fitur
     */
    public String getFitur() {
        return fitur;
    }

    /**
     * @param fitur the fitur to set
     */
    public void setFitur(String fitur) {
        this.fitur = fitur;
    }

    /**
     * @return the sinyal
     */
    public String getSinyal() {
        return sinyal;
    }

    /**
     * @param sinyal the sinyal to set
     */
    public void setSinyal(String sinyal) {
        this.sinyal = sinyal;
    }

    /**
     * @return the processor
     */
    public String getProcessor() {
        return processor;
    }

    /**
     * @param processor the processor to set
     */
    public void setProcessor(String processor) {
        this.processor = processor;
    }

    /**
     * @return the warna
     */
    public String getWarna() {
        return warna;
    }

    /**
     * @param warna the warna to set
     */
    public void setWarna(String warna) {
        this.warna = warna;
    }
}

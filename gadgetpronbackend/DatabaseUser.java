/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gadgetpron;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DatabaseUser {
     
    private Connection connect() {
        // SQLite connection string
        String url = "jdbc:sqlite:C://sqlite/db/databaseuser.db";
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return conn;
    }
     
     public static void createNewDatabase(String fileName) {
 
        String url = "jdbc:sqlite:C:/sqlite/db/" + fileName;
        
        // SQL statement for creating a new table
        String sql = "CREATE TABLE IF NOT EXISTS User (\n"
                + "	id integer PRIMARY KEY,\n"
                + "	username text NOT NULL,\n"
                + "	password text NOT NULL,\n"
                +"      nama text NOT NULL\n"
                + ");";
        
        try (Connection conn = DriverManager.getConnection(url);
                Statement stmt = conn.createStatement()) {
            // create a new table
            stmt.execute(sql);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
         try (Connection conn = DriverManager.getConnection(url)) {
            if (conn != null) {
                DatabaseMetaData meta = conn.getMetaData();
                System.out.println("The driver name is " + meta.getDriverName());
                System.out.println("A new database has been created.");
            }
 
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
     public void login(String username, String password){
         boolean Status = false;
         String unData = null;
         String PwdData = null;
             
         String sql = "Select username,password FROM User where username = " +"'"+ username+ "'";
       
         try (Connection conn = this.connect();
             Statement stmt  = conn.createStatement();
             ResultSet rs    = stmt.executeQuery(sql)){
            
            // loop through the result set
             System.out.println(rs.getString("username") + "\t" +
                                rs.getString("password")
             );
             unData=rs.getString("username");
             PwdData=rs.getString("password");
         
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }  
         if(username.equals(unData) && password.equals(PwdData)){
             System.out.println("Betul");
         }
         else{
             System.out.println("salah");
         }
     }
     
     public void SignUp(String username, String password, String nama) {
        String sql = "INSERT INTO User(username,password,nama) VALUES(?,?,?)";
 
        try (Connection conn = this.connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, username);
            pstmt.setString(2, password);
            pstmt.setString(3, nama);
            pstmt.executeUpdate();
            System.out.println("Data berhasil di tambah");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
     public static void main(String[] args) {
    }
}
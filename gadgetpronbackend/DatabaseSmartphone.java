/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gadgetpron;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
/**
 *
 * @author Nathor003
 */
public class DatabaseSmartphone {
    
             
          private Connection connect() {
        // SQLite connection string
        String url = "jdbc:sqlite:C://sqlite/db/datasmartphone.db";
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return conn;
    }
    
    public static void createNewDatabase(String fileName) {
 
        String url = "jdbc:sqlite:C:/sqlite/db/" + fileName;
        
        // SQL statement for creating a new table
        String sql = "CREATE TABLE IF NOT EXISTS Smartphone (\n"
                + "	id integer PRIMARY KEY,\n"
                + "	merek text NOT NULL,\n"
                + "	seri text NOT NULL,\n"
                +"      kamera text NOT NULL,\n"
                +"      ram text NOT NULL,\n"
                +"      processor text NOT NULL,\n"
                +"      battery   text NOT NULL,\n"
                +"      layar text NOT NULL,\n"
                +"      warna text NOT NULL,\n"
                +"      harga text NOT NULL,\n"
                +"      fitur text NOT NULL,\n"
                +"      sinyal text NOT NULL\n"
                + ");";
        
        try (Connection conn = DriverManager.getConnection(url);
                Statement stmt = conn.createStatement()) {
            // create a new table
            stmt.execute(sql);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
         try (Connection conn = DriverManager.getConnection(url)) {
            if (conn != null) {
                DatabaseMetaData meta = conn.getMetaData();
                System.out.println("The driver name is " + meta.getDriverName());
                System.out.println("A new database has been created.");
            }
 
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

    }
    
    public void selectAll(){
        String sql = "SELECT merek, seri, kamera,ram,processor,battery,layar,"
                + "warna,harga,fitur,sinyal FROM Smartphone";
        
        try (Connection conn = this.connect();
             Statement stmt  = conn.createStatement();
             ResultSet rs    = stmt.executeQuery(sql)){
            
            // loop through the result set
            while (rs.next()) {
                System.out.println(rs.getString("merek") +  "\t" + 
                                   rs.getString("seri") + "\t" +
                                   rs.getString("kamera") + "\t" +
                                   rs.getString("ram") + "\t" +
                                   rs.getString("processor") + "\t" +
                                   rs.getString("battery") + "\t" +
                                   rs.getString("layar") + "\t" +
                                   rs.getString("warna") + "\t" +
                                   rs.getString("harga") + "\t" +
                                   rs.getString("fitur") + "\t" +
                                   rs.getString("sinyal"));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        
    }
    
    public void selectItem(String nama){
        String sql = "Select merek, seri, kamera,ram,processor,battery,layar,"
                + "warna,harga,fitur,sinyal FROM Smartphone WHERE nama LIKE "+ "'%"+nama+"'";
         try (Connection conn = this.connect();
             Statement stmt  = conn.createStatement();
             ResultSet rs    = stmt.executeQuery(sql)){
            
            // loop through the result set
                System.out.println(rs.getString("merek") +  "\t" + 
                                   rs.getString("seri") + "\t" +
                                   rs.getString("kamera") + "\t" +
                                   rs.getString("ram") + "\t" +
                                   rs.getString("processor") + "\t" +
                                   rs.getString("battery") + "\t" +
                                   rs.getString("layar") + "\t" +
                                   rs.getString("warna") + "\t" +
                                   rs.getString("harga") + "\t" +
                                   rs.getString("fitur") + "\t" +
                                   rs.getString("sinyal") + "\t" );

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    public void insert(String merek, String seri, String kamera,String ram,String processor
    ,String battery,String layar,String warna,String harga,String fitur,String sinyal) {
        String sql = "INSERT INTO Smartphone(merek,seri,kamera,ram,processor,"
                + "battery,layar,warna,harga,fitur,sinyal) VALUES(?,?,?,?,?,?,?,?,?,?,?)";
 
        try (Connection conn = this.connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, merek);
            pstmt.setString(2, seri);
            pstmt.setString(3, kamera);
            pstmt.setString(4, ram);
            pstmt.setString(5, processor);
            pstmt.setString(6, battery);
            pstmt.setString(7, layar);
            pstmt.setString(8, warna);
            pstmt.setString(9, harga);
            pstmt.setString(10, fitur);
            pstmt.setString(11,sinyal);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
      
    public void delete(String seri) {
        String sql = "DELETE FROM Smartphone WHERE seri = ?";
 
        try (Connection conn = this.connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
 
            // set the corresponding param
            pstmt.setString(1, seri);
            // execute the delete statement
            pstmt.executeUpdate();
 
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
      
    public void update(String merek, String seri, String kamera,String ram,String processor
    ,String battery,String layar,String warna,String harga,String fitur,String sinyal,
    String seri2) {
        String sql = "UPDATE Smartphone SET "
                + "merek = ? , "
                + "seri = ? ,"
                +"kamera = ?,"
                +"ram = ?,"
                +"Processor = ?,"
                +"battery = ?,"
                +"layar = ?,"
                +"warna = ?,"
                +"harga = ?,"
                +"fitur= ?,"
                +"sinyal = ?"
                + "WHERE seri = ?";
 
        try (Connection conn = this.connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
 
            // set the corresponding param
            pstmt.setString(1, merek);
            pstmt.setString(2, seri);
            pstmt.setString(3, kamera);
            pstmt.setString(4, ram);
            pstmt.setString(5, processor);
            pstmt.setString(6, battery);
            pstmt.setString(7, layar);
            pstmt.setString(8, warna);
            pstmt.setString(9, harga);
            pstmt.setString(10, fitur);
            pstmt.setString(11, sinyal);
            pstmt.setString(12, seri2);
            // update 
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
      
     public static void main(String[] args) {
        createNewDatabase("datasmartphone.db");
    }
}
